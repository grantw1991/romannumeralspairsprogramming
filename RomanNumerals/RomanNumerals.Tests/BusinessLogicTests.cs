﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using NUnit.Framework;

namespace RomanNumerals.Tests
{
    [TestFixture]
    public class BusinessLogicTests
    {
        private BusinessLogic _businessLogic;

        [OneTimeSetUp]
        public void TestSetup()
        {
            _businessLogic = new BusinessLogic();
        }

        [Test]
        public void ConvertNumberToRomanNumeral_Zero_Should__Return_EmptyString()
        {
            _businessLogic.ConvertNumberToRomanNumeral(0).Should().BeEquivalentTo(string.Empty);
        }

        [Test]
        public void ConvertNumberToRomanNumeral_Negative_Should__Return_EmptyString()
        {
            _businessLogic.ConvertNumberToRomanNumeral(-100).Should().BeEquivalentTo(string.Empty);
        }

        [TestCase(1, ExpectedResult = "I")]
        [TestCase(2, ExpectedResult = "II")]
        [TestCase(3, ExpectedResult = "III")]
        [TestCase(4, ExpectedResult = "IV")]
        [TestCase(5, ExpectedResult = "V")]
        [TestCase(6, ExpectedResult = "VI")]
        [TestCase(8, ExpectedResult = "VIII")]
        [TestCase(9, ExpectedResult = "IX")]
        [TestCase(10, ExpectedResult = "X")]
        [TestCase(11, ExpectedResult = "XI")]
        [TestCase(13, ExpectedResult = "XIII")]
        [TestCase(39, ExpectedResult = "XXXIX")]
        [TestCase(40, ExpectedResult = "XL")]
        [TestCase(49, ExpectedResult = "XLIX")]
        [TestCase(50, ExpectedResult = "L")]
        [TestCase(59, ExpectedResult = "LIX")]
        [TestCase(60, ExpectedResult = "LX")]
        [TestCase(89, ExpectedResult = "LXXXIX")]
        [TestCase(90, ExpectedResult = "XC")]
        [TestCase(100, ExpectedResult = "C")]
        [TestCase(110, ExpectedResult = "CX")]
        [TestCase(115, ExpectedResult = "CXV")]
        [TestCase(160, ExpectedResult = "CLX")]
        [TestCase(500, ExpectedResult = "D")]
        [TestCase(600, ExpectedResult = "DC")]
        [TestCase(612, ExpectedResult = "DCXII")]
        [TestCase(550, ExpectedResult = "DL")]
        [TestCase(530, ExpectedResult = "DXXX")]
        [TestCase(707, ExpectedResult = "DCCVII")]
        [TestCase(900, ExpectedResult = "CM")]
        [TestCase(1000, ExpectedResult = "M")]
        [TestCase(2000, ExpectedResult = "MM")]
        [TestCase(910, ExpectedResult = "CMX")]
        [TestCase(2799, ExpectedResult = "MMDCCXCIX")]
        public string ConvertNumberToRomanNumeral_Should_Return_The_Correct_RomanNumber(int number)
        {
            return _businessLogic.ConvertNumberToRomanNumeral(number);
        }
    }
}
